﻿using bpo_master_helper.MasterModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace bpo_master_helper
{
    public class MasterHelp
    {
        public static List<AddressModel> GetDataAddress()
        {
            try
            {
                var str = DecompressString(File.ReadAllText("MasterData/master-address.txt"));
                var res = JsonConvert.DeserializeObject<List<AddressModel>>(str);
                return res;
            }
            catch
            {
                return new List<AddressModel>();
            }
        }

        public static List<AddressModel> GetDataZipcode()
        {
            try
            {
                var str = DecompressString(File.ReadAllText("MasterData/master-zipcode.txt"));
                var res = JsonConvert.DeserializeObject<List<AddressModel>>(str);
                return res;
            }
            catch
            {
                return new List<AddressModel>();
            }
        }

        public static List<NameModel> GetDataLastName()
        {
            try
            {
                var str = DecompressString(File.ReadAllText("MasterData/master-lastname.txt"));
                var res = JsonConvert.DeserializeObject<List<NameModel>>(str);
                return res;
            }
            catch { return new List<NameModel>(); }
        }

        public static List<NameModel> GetDataFirstName()
        {
            try
            {
                var str = DecompressString(File.ReadAllText("MasterData/master-firstname.txt"));
                var res = JsonConvert.DeserializeObject<List<NameModel>>(str);
                return res;
            }
            catch { return new List<NameModel>(); }

        }

        public static List<NamnhatModel> GetDataNamNhat()
        {
            try
            {
                var str = DecompressString(File.ReadAllText("MasterData/master-namnhat.txt"));
                var res = JsonConvert.DeserializeObject<List<NamnhatModel>>(str);
                return res;
            }
            catch { return new List<NamnhatModel>(); }
        }

        #region Compress-DeCompress
        /// <summary>
        /// Compresses the string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string CompressString(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        /// <summary>
        /// Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string DecompressString(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        #endregion

    }
}
