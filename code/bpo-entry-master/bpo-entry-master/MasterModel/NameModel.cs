﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bpo_master_helper.MasterModel
{
    public class NameModel
    {
        public string no { get; set; }
        public string kanjiname { get; set; }
        public string kananame { get; set; }
    }
}
