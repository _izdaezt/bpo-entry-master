﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bpo_master_helper.MasterModel;
using ClosedXML.Excel;
using Newtonsoft.Json;

namespace create_master_file
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        public void run2()
        {
            var str = File.ReadAllText("MasterData/master-namnhat.txt");
            var str2 =bpo_master_helper.MasterHelp.CompressString(str);
            File.WriteAllText("MasterData/master-namnhat2.txt", str2);
        }
        public void run()
        {

            var path = "";
            var filenam = Path.GetFileName(path);
            var dir = Path.GetDirectoryName(path);
            List<AddressModel> add = new List<AddressModel>();
            List<NameModel> name = new List<NameModel>();
            List<NamnhatModel> namnhat = new List<NamnhatModel>();
            List<AddressModel> zip = new List<AddressModel>();

            // MessageBox.Show(theDialog.FileName.ToString());
            using (XLWorkbook excelWorkbook = new XLWorkbook(path))
            {
                var nonEmptyDataRows = excelWorkbook.Worksheet(1).RowsUsed();
                var i = 1;
                foreach (var dataRow in nonEmptyDataRows)
                {
                    //name.Add(new NameModel()
                    //{
                    //    no = i,
                    //    kananame = dataRow.Cell(1).Value.ToString(),
                    //    kanjiname = dataRow.Cell(2).Value.ToString().Trim()
                    //});

                    //zip.Add(new AddressModel()
                    //{
                    //    zipcode = dataRow.Cell(1).Value.ToString().Trim(),
                    //    address1 = dataRow.Cell(2).Value.ToString().Trim(),
                    //    address2 = dataRow.Cell(3).Value.ToString().Trim(),
                    //    address3 = dataRow.Cell(4).Value.ToString().Trim()
                    //});
                    //var t = dataRow.Cell(1).Value.ToString().Trim();
                    namnhat.Add(new NamnhatModel()
                    {
                        year = (dataRow.Cell(1).Value.ToString().Trim()),
                        niendai = dataRow.Cell(2).Value.ToString().Trim(),
                        yearJapan = dataRow.Cell(3).Value.ToString().Trim()
                    });
                    i++;
                    //lstImport.Add(new AccountModel()
                    //{
                    //    id = 0,
                    //    Account = dataRow.Cell(2).Value.ToString(),
                    //    FullName = dataRow.Cell(3).Value.ToString(),
                    //    Password = dataRow.Cell(4).Value.ToString(),
                    //});
                }
                var str = JsonConvert.SerializeObject(namnhat);
                File.WriteAllText(dir + "\\" + filenam + ".txt", str);
                //InsertListUser
                // string str = ApiClient.Instance.PostPrc("api/Account/InserListtUser", lstImport);
                //MessageBox.Show(str);
            }
        }
    }
}
